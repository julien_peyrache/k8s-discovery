#!/bin/bash
echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "-------------------------------------"
echo "!!!     It will empty your node   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then
echo "Clean persistent volumes"
kubectl delete pv --all

echo "Clean volume claims"
kubectl delete pvc --all

echo "Clean pods"
kubectl delete pod --all

echo "Clean services"
kubectl delete service --all

echo "Clean deployments"
kubectl delete deployment --all

else
    echo "Ok then..."
    exit
fi


