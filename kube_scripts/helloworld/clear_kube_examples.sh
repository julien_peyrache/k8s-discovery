#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Clean persistent volumes"
kubectl delete pv helloworld-pv-volume

sleep 2

echo "Clean volume claims"
kubectl delete pvc helloworld-pv-claim

sleep 2

echo "Clean pods"
kubectl delete pod helloworld-nginx-kube

sleep 2

echo "Clean services"
kubectl delete service helloworld-nginx-service

else
exit
fi