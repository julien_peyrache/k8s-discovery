#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

mkdir -p /tmp/helloworld_kubetest
echo "Creating nginx mapped root"
echo "<H1>Hello world for test Kube</H1>" > /tmp/helloworld_kubetest/index.html

echo "Create nginx pod"
kubectl create -f nginxpod.yaml
sleep 2

echo "Create nginx service"
kubectl create -f nginxservice.yaml
sleep 2

echo "Create Persistent Volume"
kubectl create -f pvnginx.yaml
sleep 2

echo "Create Volume claim"
kubectl create -f pvclaim.yaml

sleep 5

echo "You can access to this instance with the following address"
kubectl describe service helloworld-nginx-service | grep -n "Endpoints"
echo "Warning : The port is not 8889. The service is exposed on port 80. I do not understand why"
echo "You should see a custom HTML Page from /tmp/helloworld_kubetest/index.html"

else
exit