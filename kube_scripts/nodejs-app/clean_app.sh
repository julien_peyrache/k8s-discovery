#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Delete volume for node content"
kubectl delete pv node-content-volume

echo "Delete claim for node content"
kubectl delete pvc node-content-claim

echo "Delete node deployment "
echo "It terminates associated pods as well"
kubectl delete deployment node-app
kubectl delete service node-service
sleep 2
echo "Deleting objects is asynchronous. Please have a look to dashboard to follow activity."

echo "Clean /tmp node test content"
rm -rf /tmp/node_content

else
exit
fi