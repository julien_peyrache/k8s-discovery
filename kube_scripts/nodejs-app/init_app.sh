#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "--- Begin :" $(date '+%Y-%m-%d %H:%M:%S')

echo "Mock content in /tmp from node_content - Clear - Create node content locally"
#Do not overwrite the file -- reinit won't switch
mkdir -p /tmp/node_content
cp -R node-content/* /tmp/node_content >> /dev/null

echo "Create volume/claim for node code"
kubectl apply -f node_contentvolume.yaml
kubectl apply -f node_contentvolumeclaim.yaml

echo "Deploying node container pod from public docker repository"
kubectl apply -f nodedeployment.yaml
kubectl apply -f nodeservice.yaml
sleep 2

echo "--- End :" $(date '+%Y-%m-%d %H:%M:%S')
sleep 20

echo "You can access to this instance with the following address :"
kubectl describe service node-service | grep -n "Endpoints"
echo "You shoud see helloworld from app.js"

else
exit
fi