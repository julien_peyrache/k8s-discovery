const express = require('express');
const app = express();

app.get('/', function(req, res) 
    { 
         res.header('X-Container','node-k8s');
         res.send('Hello World from node !!');
    });
app.listen(80, () => console.log('Example app listening on port 80!'));