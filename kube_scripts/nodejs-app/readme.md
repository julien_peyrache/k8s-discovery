# Running a NodeJs application within a standard container. Install from custom code, build and launch within 20 seconds

----

This section is about deployment of a simple NodeJs application and usage of
standard public images provided by Node in our case *node/alpine* :

Script : ```init_app.sh```


- Create a container (deployment + service artefact of kubernetes) from an official NodeJs image and run the code from ```node-content```

Check if it works :

Go on kubernetes dashboard : get the endpoint IP of your "node-service"

```curl http://#endpointIP#```

You shoud see the content of the version of PHP and the date time

With ```curl -v``` you should see a special header from app.js :

```X-Container:node-k8s```

You can clear all created objects by :

```clean_app.sh```

Next step :

- Try to use ingress to expose services
- Plug a mongodb cluster

----
