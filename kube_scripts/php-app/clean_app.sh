#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Delete volume for nginx configuration"
kubectl delete pv php-nginx-conf-volume

echo "Delete claim for nginx configuration"
kubectl delete pvc php-nginx-conf-claim

echo "Delete volume for php content"
kubectl delete pv php-content-volume

echo "Delete claim for php shared content"
kubectl delete pvc php-content-claim

echo "Create pod and service for frontend"
kubectl delete pod nginx-php
kubectl delete service nginx-php-service

echo "Delete fpm deployment for php7"
echo "It terminates associated pods as well"
kubectl delete deployment php7-fpm
kubectl delete service php7-fpm
sleep 2

echo "Delete fpm deployment for php5"
echo "It terminates associated pods as well"
kubectl delete deployment php5-fpm
kubectl delete service php5-fpm
sleep 2

echo "Deleting objects is asynchronous. Please have a look to dashboard to follow activity."

echo "Clean /tmp nginx configuration"
rm -rf /tmp/php_nginxconf

echo "Clean /tmp php test content"
rm -rf /tmp/php_content

else
exit
fi