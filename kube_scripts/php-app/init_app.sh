#!/bin/bash

ngconf=php_nginxconf
phpver=v7

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Which version of PHP do you want to use ?" 
echo "[1] - PHP v5.3"
echo "[2] - PHP v7"
read -r -p "Your choice: " phpanswer

if [[ "$phpanswer" =~ ^([1])+$ ]]
then
phpver=v5
else
phpver=v7
fi

echo "--- Begin :" $(date '+%Y-%m-%d %H:%M:%S')

echo "Mock Configuration in /tmp from $ngconf - Clear - Create configuration locally"
rm -rf /tmp/$ngconf
mkdir -p /tmp/$ngconf
cp -R -f ../$ngconf/$phpver/* /tmp/$ngconf

echo "Mock content in /tmp from php_content - Clear - Create php content locally"
#Do not overwrite the file -- reinit won't switch
mkdir -p /tmp/php_content
cp -R ../php_content/* /tmp/php_content >> /dev/null

echo "Create volume/claim for nginx configuration"
kubectl apply -f php_nginxconfvolume.yaml
kubectl apply -f php_volumeclaim.yaml

echo "Create volume/claim for php shared content"
kubectl apply -f php_contentvolume.yaml
kubectl apply -f php_contentvolumeclaim.yaml

echo "Deploying php ($phpver)-fpm container pod from public docker repository"
kubectl apply -f fpm/$phpver/fpmdeployment.yaml
kubectl apply -f fpm/$phpver/fpmservice.yaml
sleep 2

echo "Create pod and service for frontend (nginx-php-service)"
#Brutal but it switches
kubectl replace --force -f nginxpod.yaml
#Improvment call a nginx -reload
kubectl apply -f nginxservice.yaml

echo "--- End :" $(date '+%Y-%m-%d %H:%M:%S')
sleep 5

echo "You can access to this instance with the following address :"
kubectl describe service nginx-php-service | grep -n "Endpoints"
echo "index.html for static content and index.php for php execution"

else
exit
fi