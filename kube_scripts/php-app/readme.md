# Nginx/PHP-FPM within two containers within the ability to switch within seconds

----

This section is about deployment from public repositories images :

Script : ```init_app.sh```

- Setup an all environment with nginx and php7-fpm or php5-fpm on a development environment within 9 seconds
- Switch version of PHP by invoking ```init_app.sh``` again and choosing another one version of PHP

Almost state of art of component approach:

- through two containers (with standard configuration)
- with custom nginx configuration
- with custom code : in our case two simple index.html and index.php
- configuration and content out of containers mounted on separated volumes

Check if it works :

Go on kubernetes dashboard : get the endpoint IP of your "nginx-php-service"

```curl http://#endpointIP#/index.html```

You shoud see the content of php_content/index.html.

```curl http://#endpointIP#/index.php```

You shoud see the content of the version of PHP and the date time

With ```curl -v``` you should see a special header from php_nginxconf you choose:

- ```X-Container: nginx-linked-with-php7-fpm```
- ```X-Container: nginx-linked-with-php5-fpm```

You can clear all created objects by :

```clean_app.sh```

Next step :

- Put PHP and FPM configuration files out from container and mount volumes
- Try to use ingress to expose services

----
