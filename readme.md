# Kubernetes concepts and runnable examples on your workstation
# Code and scripts as infrastructure.
----

Understanding a simple deployment :

- Creating and exposing a simple nginx pod and service with terraform scripts
- Creating and exposing a simple nginx "hello-world" page with native k8s scripts

Understanding container orchestration :

- managing isolation, configuration, volumes, lifecycle
- managing communication between pods (classical frontend/backend)
- tackle with versions of components and be able to switch easily :
    
    - for example be able to switch versions of php within seconds

You'll find several "real life " examples below :

- How to work with nginx in less than 10 seconds
- How to work with nginx and choose PHP version in less than 30 seconds
- How to work with NodeJs in less than 10 seconds

More examples to come.

----
## The environment - Details of my sandbox's environment

[Technical Details of the environment](./techdetails.md)

Warning : Please use an isolated sandbox.

----

## Launch the dashboard

```kubectl proxy```

## Purposes' of this repository

This is a sandbox based on runnable examples to help you to understand concepts of k8s

## Runnable content

### Playing with terraform scripts and a simple nginx container
Folder: ```tf_scripts/welcomenginx```

Script: ```init_from_terraform.sh```

It creates a pod and service so you can browse a simple welcome nginx page

**If you want to destroy only objects created by init from terraform :**

Script: ```clear_terraform_examples.sh```

### Playing with kubernetes artefacts, nginx and static index.html content
Folder: ```kube_scripts/helloworld```

The goal is to create a pod and service nginx and be able to serve shared content.

Script: ```init_from_kube.sh```

- Creates an index.html in /tmp
- Creates pods, services, persistent volumes

**If you want to destroy only objects created by init from kube :**

Script: ```clear_kube_examples.sh```


### Playing with kubernetes artefacts, a dedicated node container, NodeJs code and deployment

Folder: ```kube_scripts/nodejs-app```

Creating and deploying a NodeJs simple application within a dedicated container in 20 seconds

- Deploy the code in a mock shared folder
- Create the standard node container and apply npm install, serve the content with express

More information in [```kube_scripts/nodejs-app/readme.md```](kube_scripts/nodejs-app/readme.md)


### Playing with kubernetes deployments, nginx and backend php7/php5, fpm, public docker images

Folder: ```kube_scripts/php-app```

Be able to switch between versions of PHP (it could be anything else), by code just by managing the only container we should manage.

Scenario :

- Initializing a fullstack php (nginx, fpm) within 9 seconds
- Change version of PHP by running a single script within 30 seconds
- All is open source, forkable

More information in [```kube_scripts/php-app/readme.md```](kube_scripts/php-app/readme.md)

### The erase all script

```cleanall.sh```

It uses the --all option of kubernetes. 
It is a **killing** feature.
That's why you should better use a sandbox.

- Delete pods, services, persistent volumes, claims, deployments

***Be careful, it will empty your kubernetes node by doing this***


## Notes

- This tutorial does not talk about code versioning for the moment. The code is mocked in /tmp.
- Create and destroy performance and speed depends on your machine.
- Before launching init again, check if objects are really destroyed.

Check specially state of pods (Terminating but not deleted yet) :

```kubectl get pods```


## Issues :

- When I wanted to use terraform for persistent volumes and claims, it was pending.
Because of storageClass.
- It's not possible to manage "storageClass" for object kubernetes_persistent_volume in Terraform
https://www.terraform.io/docs/providers/kubernetes/r/persistent_volume.html

- It is implemented in k8s api native scripts.

- Understand why both in terraform or kube scripts, ports other than 80 are configured but never used

## Next steps :
- Fpm configuration out of containers
- Test Ingress functionnality is in k8s API but not present in Terraform
https://github.com/terraform-providers/terraform-provider-kubernetes/issues/14

To use it, it seems we need a real loadbalancer.

## Misc
Feel free to fork and to contact me if you want.

Best regards,

Julien
