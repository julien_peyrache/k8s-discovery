**Install your own environment first**

You can follow a tutorial to setup a minikube or use a docker.
I have not tested DoD (Docker in Docker) 
I use a virtualized Debian Stretch vagrant machine.

All the code behind is a tutorial :

***Please use it in an isolated sandbox***

----

**Hosting system for k8s node : Debian Stretch**

----

**Kubernetes**

> Client Version: version.Info{Major:"1", Minor:"8", GitVersion:"v1.8.3", GitCommit:"f0efb3cb883751c5ffdbe6d515f3cb4fbe7b7acd", GitTreeState:"clean", BuildDate:"2017-11-08T18:39:33Z", GoVersion:"go1.8.3", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"8", GitVersion:"v1.8.0", GitCommit:"0b9efaeb34a2fc51ff8e4d34ad9bc6375459c4a4", GitTreeState:"dirty", BuildDate:"2017-10-17T15:09:55Z", GoVersion:"go1.8.3", Compiler:"gc", Platform:"linux/amd64"}

----
**minikube version: v0.23.0**

**Docker version :**
> Client:
 Version:      17.05.0-ce
 API version:  1.29
 Go version:   go1.7.5
 Git commit:   89658be
 Built:        Thu May  4 22:09:06 2017
 OS/Arch:      linux/amd64

>Server:
 Version:      17.05.0-ce
 API version:  1.29 (minimum version 1.12)
 Go version:   go1.7.5
 Git commit:   89658be
 Built:        Thu May  4 22:09:06 2017
 OS/Arch:      linux/amd64
 Experimental: false

----

**Terraform v0.10.8**

----

## Validate your environment

```kubectl cluster-info```

>Kubernetes master is running at https://127.0.0.1:8443

```kubectl config view```
>apiVersion: v1
>clusters:
>cluster:
>    certificate-authority: /home/youruser/.minikube/.minikube/ca.crt
>    server: https://127.0.0.1:8443
  name: minikube
contexts:
context:
    cluster: minikube
    user: minikube
  name: minikube
current-context: minikube
kind: Config
preferences: {}
users:
name: minikube
  user:
    client-certificate: /home/youruser/.minikube/.minikube/client.crt
    client-key: /home/youruser/.minikube/.minikube/client.key

***More cheats on:***
https://kubernetes.io/docs/user-guide/kubectl-cheatsheet/
