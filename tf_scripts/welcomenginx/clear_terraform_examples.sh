#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"
read -r -p "!!! Do you want to continue? [y/N] !!!" response

if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Clean pods"
kubectl delete pod welcome-terraform-test-pod
sleep 2

echo "Clean services"
kubectl delete service welcome-terraform-test-service
sleep 2

else
exit
fi
