#!/bin/bash

echo "-------------------------------------"
echo "!!! This is ONLY for test purpose !!!"
echo "!!!      Do not use in prod       !!!"
echo "!!!    Use a localkube/minikube   !!!"
echo "-------------------------------------"

read -r -p "!!! Do you want to continue? [y/N] !!!" response
if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]
then

echo "Clean former plans artefacts"
rm -f result.tplan
rm -f *.tfstate
rm -f *.tfstate.*

echo "Terraform init"
terraform init

echo "Create plan from tf scripts"
terraform plan -out result.tplan

echo "Apply the plan in kubernetes"
terraform apply result.tplan

echo "You can access to this instance with the following address"
kubectl describe service welcome-terraform-test-service | grep -n "Endpoints"
echo "Warning : The port is not 8888. The service is exposed on port 80. I do not understand why"
echo "You should see an nginx welcome page"

else
exit
fi