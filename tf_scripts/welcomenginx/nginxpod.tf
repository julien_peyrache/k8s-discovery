resource "kubernetes_pod" "nginx" {
  metadata {
    name = "welcome-terraform-test-pod"
    labels {
      App = "welcome-terraform-test"
    }
  }
  spec {
    container {
      image = "nginx:1.7.8"
      name  = "welcome-terraform-test-nginx"
      port {
        host_port = 8888
        container_port = 8888
        protocol = "TCP"
      }
  }
  restart_policy = "Never"
}
}
