resource "kubernetes_service" "nginx" {
  metadata {
    name = "welcome-terraform-test-service"
  }
  spec {
    selector {
      App = "welcome-terraform-test"
    }
    port {
      name = "http"
      protocol = "TCP"
      #Internalendpoint - Shoud serve on
      port = 8888
      node_port = 30088
      
      ##What is that for ? 
      #ServiceEndpoint
      #target_port = 8082
    }
    type = "NodePort"
  }
}
